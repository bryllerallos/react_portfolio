import React, { useEffect } from 'react';
import './Project.css';
import CS1 from '../../images/cs1.png';
import CS2 from '../../images/cs2.PNG';
import CS3 from '../../images/cs3.PNG';
import Aos from 'aos';
import 'aos/dist/aos.css';



const Project = () => {

  useEffect(() => {
    Aos.init({ duration: 3000 });
  }, []);

  return (

    <div className="container" id="Project">
      <h1 className="title text-center" data-aos="zoom-out" data-aos-easing="linear">Projects</h1>
      <div className="row w-100">

          <div className="col-md-4" data-aos="flip-left"
                            data-aos-offset="500"
                            data-aos-easing="ease-in-sine">
            <div className="card">
                <img src={CS1} alt="Zinc Metalate" className="card-image"/>
                <div className="card-body">
                  <h4 className="card-title">Zinc Metalate</h4>
                  <p className="card-title">Designed to promote the product in the farming industry</p>
                  <p className="card-title">Created from HTML and CSS</p>
                  <a href="https://bryllerallos.gitlab.io/capstone-1-final//" type="button" target="_blank rel=noreferrer" className="btn btn-grad">View Project</a>
                </div>
            </div>
          </div>
        

          <div className="col-md-4" data-aos="flip-right"
                            data-aos-offset="500"
                            data-aos-easing="ease-in-sine">
            <div className="card">
                <img src={CS2} alt="Parking Management" className="card-image"/>
                <div className="card-body">
                  <h4 className="card-title">Parking Management</h4>
                  <p className="card-title">Created this project to lessen common parking problems</p>
                  <p className="card-title">Built from Laravel Framework</p>
                  <a href="https://parking-laravel.herokuapp.com/" type="button" target="_blank rel=noreferrer" className="btn btn-grad">View Project</a>
                </div>
            </div>
          </div>

          <div className="col-md-4" data-aos="flip-left"
                            data-aos-offset="500"
                            data-aos-easing="ease-in-sine">
            <div className="card">
                <img src={CS3} alt="Parking Management" className="card-image"/>
                <div className="card-body">
                <h4 className="card-title">Course Booking</h4>
                  <p className="card-title">Designed to help individuals who wants to learn new things</p>
                  <p className="card-title">Built using MEN Stack and Javascript</p>
                  <a href="https://bryllerallos.gitlab.io/capstone-2_project/" type="button" target="_blank rel=noreferrer" className="btn btn-grad">View Project</a>
                </div>
            </div>
          </div>
      </div>
    </div>
  )
}

export default Project
