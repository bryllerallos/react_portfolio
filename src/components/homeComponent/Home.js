import React, {useEffect} from 'react';
import './Home.css';
import sy from '../../images/sy.png';
import Aos from 'aos';
import 'aos/dist/aos.css';


const Home = () => {

    useEffect(() => {
    Aos.init({ duration: 1200 });
     }, []);

    return(
        <>
            <div id="Home" className="container my-5" data-aos="fade-out">
                <div className="row d-flex my-4">
                    <div className="col-lg-6" id='img-div'>
                        <img src={sy} alt="sy" id="sy" title="Welcome to My Page" className=''/>
                    </div>
                    <div className="col-lg-6" id='home-content'>
                        <h4 className='about type'>Hi Im Brylle. An aspiring web developer aiming to build creative and useful websites.</h4>
                       <p className="lead"><em><q><em>Embrace your uniqueness. Time is much too short to be living someone else's life.</em></q> --Kobi Yamada</em></p>
                    </div>
                </div>
            </div>
        </>
    )
}


export default Home