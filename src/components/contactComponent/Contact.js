import React, { useState, useEffect } from 'react';
import './Contact.css';
import {Form, Button} from 'react-bootstrap';
import Aos from 'aos';
import 'aos/dist/aos.css';
import * as MdIcons from 'react-icons/md'
import * as IoIcons from 'react-icons/io'
import * as ImIcons from 'react-icons/im'
import * as AiIcons from 'react-icons/ai'
import emailjs from 'emailjs-com';


const Contact = () => {

    const [title, setTitle] = useState('Contact Me')
    const [active, setActive] =useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('')


    useEffect(()=> {
        if((name !== '' && email !== '' && message !== '')){
            setActive(true)
        }else {
            setActive(false)
        }
    }, [name,email,message])

    const submit = e => {
        e.preventDefault();

        emailjs.sendForm('service_c0w4elc', 'template_57uobf4', e.target, 'user_tNhhVsmAdJm3BHqJtvwPk')
        .then((result) => {
            setTitle(`Thanks for your time. I'll get back to you soon!`)
            setName('')
            setEmail('')
            setMessage('')
        }, (error) => {
            setTitle(`Something Went Wrong. Please try again later!`)
            console.log(error.text);
        });
        
    }

    useEffect(() => {
        Aos.init({ duration: 1500 });
         }, []);

    return(
        <>
           <div id="Contact" className="container">
            <h1 className="text-center title" data-aos="zoom-in">{title}</h1>
               <div className="row w-100">
                    <div className="col-md-4 my-5" id='form'>
                        <Form id='form' autoComplete='off' onSubmit={submit}>
                            <Form.Group>
                               
                                <Form.Control className='form-control' type="text" placeholder="Your Name" name='name' value={name} onChange={e => setName(e.target.value)} required/>
                                <hr />
                            </Form.Group>
                            <Form.Group>
                              
                                <Form.Control className='form-control' type="email" placeholder="Your Email" name='email' value={email} onChange={e => setEmail(e.target.value)} required/>
                                <hr/>
                            </Form.Group>
                            <Form.Group>
                                
                                <textarea id='textarea' className='form-control' rows='3' name='message' placeholder="Your Message" value={message} onChange={e => setMessage(e.target.value)} required/>
                                <hr/>
                            </Form.Group>
                            {active ?
                            <Button type="submit" id="button" className="btn-grad mx-auto">Submit</Button>
                             :       
                            <Button disabled type="submit" id="button" className="btn-grad mx-auto">Submit</Button>
                             }
                        </Form>
                    </div>
                    <div className="col-md-5 offset-lg-1 py-5 text-center" id="contact-info">
                    <div className='d-block row text-center'>
                       <MdIcons.MdEmail className='icons' title='Email'/>
                       <p>rallosbrylle@gmail.com</p>
                    </div>
    
                    <div className='row d-block text-center'>
                       <IoIcons.IoMdCall className='icons' title='Mobile'/>
                       <p>+639772439384</p>
                    </div>
                    <div className='row' id="social-media">
                      <a href="https://gitlab.com/bryllerallos" target="_blank rel=noreferrer">
                        <AiIcons.AiFillGitlab className='icons' title='Gitlab'/>
                      </a>
                      <a href="https://www.linkedin.com/in/brylle-symon-rallos-951900105/" target="_blank rel=noreferrer">
                        <ImIcons.ImLinkedin className='icons' title='LinkedIn'/>
                      </a>
                       
                    </div>
                        
                    </div>
               </div>
            </div>
          
        </>
    )
}


export default Contact;