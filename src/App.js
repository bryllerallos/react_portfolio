import React, { useState, useEffect, useRef } from 'react'
import './App.css';
import Navigation from './components/navComponent/Navbar'
import Home from './components/homeComponent/Home';
import Project from './components/projectComponent/Project';
import Skills from './components/skillsComponent/Skills';
import Contact from './components/contactComponent/Contact';
import Footer from './components/footerComponent/Footer';
import WAVES from 'vanta/dist/vanta.waves.min';





  const App = (props) => {
    const [vantaEffect, setVantaEffect] = useState(0)
    const vanta = useRef(null)
    useEffect(() => {
      if (!vantaEffect) {
        setVantaEffect(WAVES({
          el: vanta.current
          
        }))
      }
      return () => {
        if (vantaEffect) vantaEffect.destroy()
      }
    }, [vantaEffect])

  
    return (
      <div className="App" ref={vanta} >

        <Navigation />
        <Home />
        <Project />
        <Skills />
        <Contact />
        <Footer />
      
      </div>
)
}



export default App;
